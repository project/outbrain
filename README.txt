Outbrain Integration
--------------------

This module integrates drupal with outbrain - http://www.outbrain.com/

You need to register with outbrian. The module provides two blocks with 
internal and external content.

INSTALLATION
============

- Enable the module
- Set permissions on the permission page (admin/people/permissions#module-outbrain_integration) 
if you want other roles to administer Ourbrain
- Configure outbrain at admin/config/system/outbrain_integration
- Display outbrain blocks on your pages (admin/structure/block)